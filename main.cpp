// https://www.youtube.com/watch?v=c2qx13d4YG4&index=41&list=PL6xSOsbVA1eaxY06L5ZZJfDZSohwO6AKY
#include <iostream>
using namespace std;


void expand(int**& arr, int& cap, int nrOfEl)
{
	// Step 1: Increase capacity
	cap *= 2;

	// Step 2: Create temp array
	int** tempArray = new int*[cap];

	// Step 3: Initialize temp array
	for (int i = 0; i < cap; ++i) {
		tempArray[i] = nullptr;
	}

	// Step 4: Copy over things from old array
	for (int j = 0; j < nrOfEl; ++j) {
		tempArray[j] = new int(*arr[j]);
	}

	// Step 5: Delete old array

	for (int k = 0; k < nrOfEl; ++k) {
		delete arr[k];
	}

	delete[] arr;

	// Step 6: Point old array to new one
	arr = tempArray;

	cout << "Array expanded! New size: " << cap << endl;

}

void add(int el, int**& arr, int& cap, int& nrOfEl)
{
	if(nrOfEl >= cap)
	{
		expand(arr, cap, nrOfEl);
	}

	arr[nrOfEl++] = new int(el);

	cout << el << " Added. nrOfEl " << nrOfEl << endl;
}


int main()
{
	int cap = 5;
	int nrOfEl = 0;
	int** arr = new int*[cap];

	// Initialize
	for (int i = 0; i < cap; ++i) {
		arr[i] = nullptr;
	}


	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);
	add(5, arr, cap, nrOfEl);

	// Print
	for (int k = 0; k < nrOfEl; ++k) {
		cout << k << " " << *arr[k] << endl;
	}


	// Delete the pointers inside the array
	for (int j = 0; j < cap; ++j) {
		delete arr[j];
	}

	// Delete the main array
	delete[] arr;

	return 0;
}